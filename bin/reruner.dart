import 'package:reruner/reruner.dart';

void main(List<String> arguments) async {
  var rr = Rerunner.FromArgs(arguments);
  rr.run();
}
A simple command-line application for restarting your tests or server after changing source files.

Tested for linux only.

Alfa-version, not for production.

First: `pub global activate reruner`
Next: `reruner` command will placed in you .pub-cache and available for call from command line.
Check this by entering command: `which reruner`.

`reruner` command has arguments: 

 - first argument - file name or directory name which you want to watch
 - second argument - command to run every time after watched file(s) was changed

 Sample: `reruner . 'ls -l'` - will look (not recursively) to current directory files 
 and print list of files on any changes.
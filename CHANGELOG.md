## 0.0.1

- Initial version, created by Stagehand

## 0.0.2

- Fix error

## 0.0.3

- Watch all files and directories listed in arguments (not recursively) before 'command' argument.

## 0.0.4

- SIGINT used

## 0.0.5

- sleep 2 before run
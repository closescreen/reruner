import 'dart:async';
import 'dart:convert';
import 'dart:io';

class Rerunner {
  List<String> namesToWatch = [];
  List<FileSystemEntity> entToWatch = [];
  Map modifyTime;
  String commandToRun;
  Process runned_process;
  ProcessResult runned_process_result;

  Rerunner.FromArgs(List<String> args) {
    if (args.isEmpty) {
      print('No args!');
      exit(2);
    }
    commandToRun = args.last;
    if (commandToRun.isEmpty) {
      print('Empty args!');
      exit(2);
    }

    namesToWatch = List.from(args.getRange(0, args.length - 1));
    if (namesToWatch.isEmpty) {
      print('Empty list of files to watch!');
      exit(2);
    }

    for (var name in namesToWatch) {
      if (FileSystemEntity.isFileSync(name)) {
        entToWatch.add(File(name));
      } else if (FileSystemEntity.isDirectorySync(name)) {
        entToWatch.add(Directory(name));
      } else {
        // other types not supported
        print('$name is not file or directory');
        exit(2);
      }
    }
  }

  Future<Event<int>> runProcess() async {
    var commandWithParams = commandToRun.split(RegExp(r'\s+'));
    var cmd = commandWithParams.first;
    var pars = List<String>.from(
        commandWithParams.getRange(1, commandWithParams.length));
    runned_process = await Process.start(cmd, pars);
    print('runned pid: ${runned_process.pid}');

    var exitCode = (await Future.wait([
      runned_process.stdout
          .transform(utf8.decoder)
          .listen((d) => stdout.write(d))
          .asFuture(), //.pipe(stdout),
      runned_process.stderr
          .transform(utf8.decoder)
          .listen((d) => stdout.write(d))
          .asFuture(), //.pipe(stderr),
      runned_process.exitCode
    ]))[2];

    print('exit code: $exitCode');
    return Event(EventType.PROCESS_EXITED, exitCode);
  }

  bool stopProcess() {
    print('stop pid:${runned_process?.pid}');
    var result = runned_process?.kill(ProcessSignal.sigint);
    // stdout.close();
    // stderr.close();
    runned_process = null;
    return result;
  }

  Future<Event<int>> restartCommand() async {
    stopProcess();
    int sec = 2;
    print('sleep $sec...');
    await Future.delayed(Duration(seconds: sec));
    print('run process...');
    return await runProcess();
  }

  Future<Event<FileSystemEvent>> filesChanged() async {
    List<Future<FileSystemEvent>> watched_entities = [];
    for (var ent in entToWatch) {
      watched_entities.add(ent.watch().first);
    }
    var fs_event = await Future.any(watched_entities);
    print('watched: $watched_entities');
    return Event(EventType.FILES_CHANGED, fs_event);
  }

  run() async {
    Event event;

    while (true) {
      do {
        print("new loop...");
        event = await Future.any([
          restartCommand(),
          filesChanged(),
        ]);
        print('Event: $event, ${event.eventType}');
      } while (event.eventType == EventType.FILES_CHANGED);

      await filesChanged();
    }
  }
}

enum EventType { FILES_CHANGED, PROCESS_EXITED }

class Event<T> {
  EventType eventType;
  T returnValue;
  Event(this.eventType, this.returnValue);
}
